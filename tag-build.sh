#!/bin/sh

npm run build
git checkout -b $1
rm -rf src
rm -rf package-lock.json
rm -rf yarn.lock
rm -rf .storybook
rm -rf stories
git add dist -f
git add .
git commit -m $1
git push origin HEAD
